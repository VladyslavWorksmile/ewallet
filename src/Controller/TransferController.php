<?php

namespace App\Controller;


use App\Entity\Transfer;
use App\Entity\User;
use App\Repository\TransferRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\Cache;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

#[Route('/')]
final class TransferController extends AbstractController
{
    #[Route('/transfer', name: 'transfer_post', methods: ['POST'])]
    #[Cache(smaxage: 10)]
    public function index(
        #[CurrentUser] ?User $user,
        Request $request,
        EntityManagerInterface $entityManager,
        TransferRepository $transferRepository
    ): Response
    {
        if ($user === null) {
            return $this->redirectToRoute('security_login');
        }
        $amount = (float)$request->request->get('amount');
        $transfer = new Transfer();
        $transfer->setUser($user)->setAmount($amount)->setCreatedAt(new \DateTime());
        $entityManager->persist($transfer);
        $entityManager->flush();
        $transfers = $transferRepository->findAll();
        $sum = 0;
        foreach ($transfers as $transfer)
        {
            $sum += (float)$transfer->getAmount();
        }


        return $this->render('index.html.twig', [
            'transfers' => $transferRepository->findAll(),
            'sum' => $sum,
            'error' => null,
            'success' => true
        ]);
    }

    #[Route('/transfer', name: 'transfer_get', methods: ['GET'])]
    #[Cache(smaxage: 10)]
    public function get(
        TransferRepository $transferRepository
    ): Response
    {
        $transfers = $transferRepository->findAll();
        $sum = 0;
        foreach ($transfers as $transfer)
        {
            $sum += (float)$transfer->getAmount();
        }

        return $this->render('index.html.twig', [
            'transfers' => $transfers,
            'sum' => $sum,
            'error' => null,
            'success' => null
        ]);
    }
}
