<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\TransferRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\Cache;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

#[Route('/')]
final class IndexController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    #[Cache(smaxage: 10)]
    public function index(
        #[CurrentUser] ?User $user,
        Request $request,
        TransferRepository $transfers
    ): Response
    {
        if ($user === null) {
            return $this->redirectToRoute('security_login');
        }
        $transfers = $transfers->findAll();
        $sum = 0;
        foreach ($transfers as $transfer)
        {
            $sum += (float)$transfer->getAmount();
        }

        return $this->render('index.html.twig', [
            'transfers' => $transfers,
            'sum' => $sum,
            'error' => null,
            'success' => null
        ]);
    }
}
